﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HRCloudTest.Models;

namespace HRCloudTest.Controllers
{
    public class ContactsController : Controller
    {
        private ContactsDBContext db = new ContactsDBContext();

        // GET: Contacts
        public ActionResult Index()
        {
            List<ContactViewModel> viewContacts = ContactViewModel.getViewContacts(ContactsDBContext.GetContacts());
            return Json(viewContacts,JsonRequestBehavior.AllowGet);
        }


        public ActionResult Filter(String searchString)
        {
            List<Contact> contacts;
            if (searchString != null && !searchString.Equals(""))
            {
                String[] searchStrings = searchString.Split(' ');
                contacts = new List<Contact>();
                foreach (String s in searchStrings)
                {
                    contacts.AddRange(from c in db.Contacts where c.name.Contains(s) || c.surname.Contains(s) select c);
                    List<int> tagIds = (from t in db.tags where t.tagText.Contains(s) select t.id).ToList();
                    List<int> contactIds = (from pair in db.pairs where tagIds.Contains(pair.tagId) select pair.contactId).ToList();
                    foreach(var id in contactIds)
                    {
                        Contact c = db.Contacts.Find(id);
                        if (!contacts.Contains(c))
                        {
                            contacts.Add(c);
                        }
                    }
                }
                foreach (Contact c in contacts)
                {
                    ContactsDBContext.UpdateContact(c);
                }
            }
            else
            {
                contacts = ContactsDBContext.GetContacts();
            }
            List<ContactViewModel> viewContacts = ContactViewModel.getViewContacts(contacts);
            return Json(viewContacts, JsonRequestBehavior.AllowGet);
        }

        // GET: Contacts/Details/5
        public ActionResult Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Contact contact = db.Contacts.Find(id);
            if (contact == null)
            {
                return HttpNotFound();
            }
            return Json(new ContactViewModel(contact), JsonRequestBehavior.AllowGet);
        }

       

        
        
        public String Create(CreateContactModel model)
        {
            if (ModelState.IsValid)
            {
                //Checking for existing phones/emails
                Phone phone = new Phone(model.phone);
                if (db.phones.ToList().Contains(phone))
                {
                    return "This phone number is already assigned to a user";
                    
                }
                Email email = new Email(model.email);
                if (db.emails.ToList().Contains(email))
                {
                    return "This email address is already assigned to a user";
                }

                //Creating contact 
                Contact contact = new Contact(model.name, model.surname, model.address);
                db.Contacts.Add(contact);
                db.SaveChanges();
                
                phone.ownerId = contact.id;
                db.phones.Add(phone);
                
                email.ownerId = contact.id;
                db.emails.Add(email);

                if (model.tags != null)
                {
                    List<Tag> currentTags = Tag.parseTags(model.tags);
                    foreach (var tag in currentTags)
                    {
                        tag.connectionAdded(contact.id);
                    
                    }
                }
                db.SaveChanges();
                return "True";
            }
            return "Error";
        }

       
        public String Edit(ContactEditModel editModel)
        {
            if (ModelState.IsValid)
            {

                Contact contact = db.Contacts.Find(editModel.id);
                contact.edit(editModel);
                db.Entry(contact).State = EntityState.Modified;
                db.SaveChanges();

                List<Tag> currentTags = new List<Tag>();
                if (contact.tags != null)
                {
                    currentTags = Tag.parseTags(contact.tags);
                    db.SaveChanges();
                    foreach (var tag in currentTags)
                    {
                        tag.connectionAdded(contact.id);

                    }
                }

                List<int> contactTagIds = (from t in db.pairs where t.contactId == contact.id select t.tagId).ToList();
                List<Tag> oldTags = db.tags.ToList().FindAll(t => contactTagIds.Contains(t.id));
                List<Tag> removedTags = oldTags.Where(tag => !currentTags.Contains(tag)).ToList();
                foreach (var tag in removedTags)
                {
                    tag.connectionLost(contact.id);
                }

                return "True";
            }
            return "All fields except tags are obligatory";

        }
  

        
        public String Delete(int id)
        {
            Contact contact = db.Contacts.Find(id);

            List<Phone> phones = (from p in db.phones where p.ownerId == contact.id select p).ToList();
            db.phones.RemoveRange(phones);

            List<Email> emails = (from e in db.emails where e.ownerId == contact.id select e).ToList();
            db.emails.RemoveRange(emails);

            List<int> tagIds = (from p in db.pairs where p.contactId == contact.id select p.tagId).ToList();
            foreach (var tagId in tagIds)
            {
                db.tags.Find(tagId).connectionLost(contact.id);
            }

            db.Contacts.Remove(contact);
            db.SaveChanges();
            return "True";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
