﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HRCloudTest.Models;

namespace HRCloudTest.Controllers
{
    public class EmailsController : Controller
    {
        private ContactsDBContext db = new ContactsDBContext();


        public ActionResult GetAll(int id)
        {
            ContactsDBContext db = new ContactsDBContext();
            return Json((from e in db.emails where e.ownerId == id select e).ToList(), JsonRequestBehavior.AllowGet);
        }

        public ActionResult Get(int id)
        {
            ContactsDBContext db = new ContactsDBContext();
            return Json(db.emails.Find(id), JsonRequestBehavior.AllowGet);
        }

        public String Create(String emailAddress, int ownerId)        {

            if (emailAddress != null && !emailAddress.Equals(""))
            {
                Email email = new Email(emailAddress);
                email.ownerId = ownerId;
                if (db.emails.ToList().Contains(email))
                {
                    return "This email address is already assigned to a user";
                }
                db.emails.Add(email);
                db.SaveChanges();
                return "True";
            }

            return "Error";
        }

        
        public String Edit(int id, String emailAddress)
        {
            if (emailAddress != null && !emailAddress.Equals(""))
            {
                if (db.emails.ToList().Select<Email, String>(e => e.emailAddress).Contains(emailAddress))
                {
                   return "This email address is already assigned to a user";
                }
                Email email = db.emails.Find(id);
                email.emailAddress = emailAddress;
                db.Entry(email).State = EntityState.Modified;
                db.SaveChanges();
                return "True";
            }
            return "Email address is obligatory.";
        }

        
        public String Delete (int id)
        {
            Email email = db.emails.Find(id);
            db.emails.Remove(email);
            db.SaveChanges();
            return "True";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
