﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Mvc;
using HRCloudTest.Models;

namespace HRCloudTest.Controllers
{
    public class PhonesController : Controller
    {
        private ContactsDBContext db = new ContactsDBContext();




        public ActionResult GetAll(int id)
        {
            ContactsDBContext db = new ContactsDBContext();
            List<Phone> phones = (from p in db.phones where p.ownerId == id select p).ToList();
            return Json(phones, JsonRequestBehavior.AllowGet);
        }

        public ActionResult Get(int id)
        {
            ContactsDBContext db = new ContactsDBContext();
            return Json(db.phones.Find(id), JsonRequestBehavior.AllowGet);
        }



        public String Create(String phoneNumber, int ownerId)
        {
            
            if (phoneNumber != null && !phoneNumber.Equals(""))
            {
                Phone phone = new Phone(phoneNumber);
                phone.ownerId = ownerId;
                if (db.phones.ToList().Contains(phone))
                {
                    return "This phone number is already assigned to a user";
                }
                db.phones.Add(phone);
                db.SaveChanges();
                return "True";
            }

            return "Error";
        }

       
        public String Edit(int id,String phoneNumber)
        {

            if (phoneNumber != null && !phoneNumber.Equals(""))
            {
                if (db.phones.ToList().Select<Phone,String>(p => p.phoneNumber).Contains(phoneNumber))
                {
                    return "This phone number is already assigned to a user";
                }
                Phone phone = db.phones.Find(id);
                phone.phoneNumber = phoneNumber;                
                db.Entry(phone).State = EntityState.Modified;
                db.SaveChanges();
                return "True";
            }
            return "Phone number is obligatory.";
        }

        
        public String Delete(int id)
        {
            Phone phone = db.phones.Find(id);
            db.phones.Remove(phone);
            db.SaveChanges();
            return "True";
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }
    }
}
