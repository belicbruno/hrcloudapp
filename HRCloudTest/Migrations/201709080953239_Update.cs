namespace HRCloudTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "name", c => c.String());
            AddColumn("dbo.Contacts", "surname", c => c.String());
            AddColumn("dbo.Contacts", "adress", c => c.String());
            AddColumn("dbo.Emails", "email", c => c.String());
            AddColumn("dbo.Emails", "isPrimary", c => c.Boolean(nullable: false));
            AddColumn("dbo.Phones", "phone", c => c.String());
            AddColumn("dbo.Phones", "isPrimary", c => c.Boolean(nullable: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.Phones", "isPrimary");
            DropColumn("dbo.Phones", "phone");
            DropColumn("dbo.Emails", "isPrimary");
            DropColumn("dbo.Emails", "email");
            DropColumn("dbo.Contacts", "adress");
            DropColumn("dbo.Contacts", "surname");
            DropColumn("dbo.Contacts", "name");
        }
    }
}
