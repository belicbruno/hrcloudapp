namespace HRCloudTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update1 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Emails", "ownerId", c => c.Int(nullable: false));
            AddColumn("dbo.Emails", "Contact_id", c => c.Int());
            AddColumn("dbo.Phones", "ownerId", c => c.Int(nullable: false));
            AddColumn("dbo.Phones", "Contact_id", c => c.Int());
            CreateIndex("dbo.Emails", "Contact_id");
            CreateIndex("dbo.Phones", "Contact_id");
            AddForeignKey("dbo.Emails", "Contact_id", "dbo.Contacts", "id");
            AddForeignKey("dbo.Phones", "Contact_id", "dbo.Contacts", "id");
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Phones", "Contact_id", "dbo.Contacts");
            DropForeignKey("dbo.Emails", "Contact_id", "dbo.Contacts");
            DropIndex("dbo.Phones", new[] { "Contact_id" });
            DropIndex("dbo.Emails", new[] { "Contact_id" });
            DropColumn("dbo.Phones", "Contact_id");
            DropColumn("dbo.Emails", "Contact_id");
        }
    }
}
