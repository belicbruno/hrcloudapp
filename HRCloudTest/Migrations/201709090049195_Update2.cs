namespace HRCloudTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update2 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.ContactTagPairs",
                c => new
                    {
                        tagId = c.Int(nullable: false, identity: true),
                        contactId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.tagId);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.ContactTagPairs");
        }
    }
}
