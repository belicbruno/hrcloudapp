namespace HRCloudTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update3 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.ContactTagPairs");
            AddColumn("dbo.Contacts", "tags", c => c.String());
            AddColumn("dbo.Emails", "emailAdress", c => c.String());
            
            AddColumn("dbo.Phones", "phoneNumber", c => c.String(nullable: false));
           
            AddColumn("dbo.Tags", "connectionNumber", c => c.Int(nullable: false));
            AlterColumn("dbo.ContactTagPairs", "tagId", c => c.Int(nullable: false));
            AddPrimaryKey("dbo.ContactTagPairs", new[] { "tagId", "contactId" });
            DropColumn("dbo.Emails", "email");
            DropColumn("dbo.Phones", "phone");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Phones", "phone", c => c.String());
            AddColumn("dbo.Emails", "email", c => c.String());
            DropPrimaryKey("dbo.ContactTagPairs");
            AlterColumn("dbo.ContactTagPairs", "tagId", c => c.Int(nullable: false, identity: true));
            DropColumn("dbo.Tags", "connectionNumber");
            DropColumn("dbo.Phones", "ownerId");
            DropColumn("dbo.Phones", "phoneNumber");
            DropColumn("dbo.Emails", "ownerId");
            DropColumn("dbo.Emails", "emailAdress");
            DropColumn("dbo.Contacts", "tags");
            AddPrimaryKey("dbo.ContactTagPairs", "tagId");
        }
    }
}
