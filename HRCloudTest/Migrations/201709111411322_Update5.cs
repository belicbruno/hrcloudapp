namespace HRCloudTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update5 : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("dbo.ContactTagPairs");
            AddColumn("dbo.ContactTagPairs", "id", c => c.Int(nullable: false));
            AlterColumn("dbo.ContactTagPairs", "tagId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.ContactTagPairs", "tagId");
            CreateIndex("dbo.ContactTagPairs", "id");
            CreateIndex("dbo.ContactTagPairs", "contactId");
            AddForeignKey("dbo.ContactTagPairs", "contactId", "dbo.Contacts", "id", cascadeDelete: true);
            AddForeignKey("dbo.ContactTagPairs", "id", "dbo.Tags", "id", cascadeDelete: true);
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.ContactTagPairs", "id", "dbo.Tags");
            DropForeignKey("dbo.ContactTagPairs", "contactId", "dbo.Contacts");
            DropIndex("dbo.ContactTagPairs", new[] { "contactId" });
            DropIndex("dbo.ContactTagPairs", new[] { "id" });
            DropPrimaryKey("dbo.ContactTagPairs");
            AlterColumn("dbo.ContactTagPairs", "tagId", c => c.Int(nullable: false));
            DropColumn("dbo.ContactTagPairs", "id");
            AddPrimaryKey("dbo.ContactTagPairs", new[] { "tagId", "contactId" });
        }
    }
}
