namespace HRCloudTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update6 : DbMigration
    {
        public override void Up()
        {
          
            DropPrimaryKey("dbo.ContactTagPairs");
            AlterColumn("dbo.ContactTagPairs", "tagId", c => c.Int(nullable: false));
            AlterColumn("dbo.ContactTagPairs", "id", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.ContactTagPairs", "id");
            CreateIndex("dbo.ContactTagPairs", "tagId");
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.ContactTagPairs");
            AlterColumn("dbo.ContactTagPairs", "id", c => c.Int(nullable: false));
            AlterColumn("dbo.ContactTagPairs", "tagId", c => c.Int(nullable: false, identity: true));
            AddPrimaryKey("dbo.ContactTagPairs", "tagId");
            RenameColumn(table: "dbo.ContactTagPairs", name: "tagId", newName: "id");
            AddColumn("dbo.ContactTagPairs", "tagId", c => c.Int(nullable: false, identity: true));
            CreateIndex("dbo.ContactTagPairs", "id");
        }
    }
}
