namespace HRCloudTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update7 : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.ContactTagPairs", newName: "TagConnections");
        }
        
        public override void Down()
        {
            RenameTable(name: "dbo.TagConnections", newName: "ContactTagPairs");
        }
    }
}
