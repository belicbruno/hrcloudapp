namespace HRCloudTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update8 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Contacts", "address", c => c.String());
            AddColumn("dbo.Emails", "emailAddress", c => c.String());
            DropColumn("dbo.Contacts", "adress");
            DropColumn("dbo.Emails", "emailAdress");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Emails", "emailAdress", c => c.String());
            AddColumn("dbo.Contacts", "adress", c => c.String());
            DropColumn("dbo.Emails", "emailAddress");
            DropColumn("dbo.Contacts", "address");
        }
    }
}
