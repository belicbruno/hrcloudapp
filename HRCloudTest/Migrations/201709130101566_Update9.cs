namespace HRCloudTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update9 : DbMigration
    {
        public override void Up()
        {
            DropForeignKey("dbo.TagConnections", "contactId", "dbo.Contacts");
            DropForeignKey("dbo.TagConnections", "tagId", "dbo.Tags");
            DropIndex("dbo.TagConnections", new[] { "tagId" });
            DropIndex("dbo.TagConnections", new[] { "contactId" });
        }
        
        public override void Down()
        {
            CreateIndex("dbo.TagConnections", "contactId");
            CreateIndex("dbo.TagConnections", "tagId");
            AddForeignKey("dbo.TagConnections", "tagId", "dbo.Tags", "id", cascadeDelete: true);
            AddForeignKey("dbo.TagConnections", "contactId", "dbo.Contacts", "id", cascadeDelete: true);
        }
    }
}
