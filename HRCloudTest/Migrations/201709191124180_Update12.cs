namespace HRCloudTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update12 : DbMigration
    {
        public override void Up()
        {
            DropTable("dbo.TagConnections");
        }
        
        public override void Down()
        {
            CreateTable(
                "dbo.TagConnections",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        tagId = c.Int(nullable: false),
                        contactId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
    }
}
