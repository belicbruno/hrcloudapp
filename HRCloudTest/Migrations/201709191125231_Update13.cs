namespace HRCloudTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update13 : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.TagConnections",
                c => new
                    {
                        id = c.Int(nullable: false, identity: true),
                        tag = c.Int(nullable: false),
                        contact = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.id);
            
        }
        
        public override void Down()
        {
            DropTable("dbo.TagConnections");
        }
    }
}
