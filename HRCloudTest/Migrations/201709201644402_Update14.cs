namespace HRCloudTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update14 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.TagConnections", "tagId", c => c.Int(nullable: false));
            AddColumn("dbo.TagConnections", "contactId", c => c.Int(nullable: false));
            DropColumn("dbo.TagConnections", "tag");
            DropColumn("dbo.TagConnections", "contact");
        }
        
        public override void Down()
        {
            AddColumn("dbo.TagConnections", "contact", c => c.Int(nullable: false));
            AddColumn("dbo.TagConnections", "tag", c => c.Int(nullable: false));
            DropColumn("dbo.TagConnections", "contactId");
            DropColumn("dbo.TagConnections", "tagId");
        }
    }
}
