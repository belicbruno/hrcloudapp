namespace HRCloudTest.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Update15 : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Tags", "tagText", c => c.String());
        }
        
        public override void Down()
        {
        }
    }
}
