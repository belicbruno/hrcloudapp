﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace HRCloudTest.Models
{
    public class Contact
    {
        [Key]
        public int id { get; set; }
        public String name { get; set; }
        public String surname { get; set; }
        public String address { get; set; }
        public List<Phone> phones { get; set; }
        public List<Email> emails { get; set; }
        public String tags { get; set; }

        public Contact()
        {
            phones = new List<Phone>();
            emails = new List<Email>();
        }

        public Contact(String name, String surname, String address)
        {
            this.name = name;
            this.surname = surname;
            this.address = address;
            phones = new List<Phone>();
            emails = new List<Email>();
        }
        public void edit(ContactEditModel model)
        {
            name = model.name;
            surname = model.surname;
            address = model.address;
            tags = model.tags;
        }
    }
}