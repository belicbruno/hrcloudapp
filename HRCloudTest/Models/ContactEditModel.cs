﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace HRCloudTest.Models
{
    public class ContactEditModel
    {
        public int id { get; set; }

        [Required]
        public string name { get; set; }
        [Required]
        public string surname { get; set; }
        [Required]
        public string address { get; set; }
        public string tags { get; set; }

     


        public ContactEditModel(Contact contact)
        {
            this.id = contact.id;
            this.name = contact.name;
            this.surname = contact.surname;
            this.address = contact.address;
            this.tags = Tag.GetContactTags(contact.id);
        }
        public ContactEditModel() { }
    }
}