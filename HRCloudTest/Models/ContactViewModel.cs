﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HRCloudTest.Models
{
    public class ContactViewModel
    {
        public int id;

        [Display(Name = "First name")]
        public string name { get; set; }

        [Display(Name = "Last name")]
        public string surname { get; set; }

        public string address { get; set; }

        public string email { get; set; }

        public string phone { get; set; }

        public string tags { get; set; }


        public ContactViewModel(Contact contact)
        {
            this.id = contact.id;
            this.name = contact.name;
            this.surname = contact.surname;
            this.address = contact.address;
            ContactsDBContext db = new ContactsDBContext();
            if (contact.emails.Count > 0)
            {           

                this.email = contact.emails.First().emailAddress;
            } else
            {
                this.email = "";
            }
            if(contact.phones.Count > 0)
            {
                this.phone = contact.phones.First().phoneNumber;
            } else
            {
                this.phone = "";
            }
            
            this.tags = Tag.GetContactTags(contact.id);

        }

        public static List<ContactViewModel> getViewContacts(List<Contact> contacts)
        {
            List<ContactViewModel> viewContacts = new List<ContactViewModel>();
            foreach(Contact c in contacts)
            {
                viewContacts.Add(new ContactViewModel(c));
            }
            return viewContacts;
        }
    }

   
}