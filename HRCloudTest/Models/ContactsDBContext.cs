﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using HRCloudApplication.Models;


namespace HRCloudTest.Models
{
    public class ContactsDBContext: DbContext
    {

        public ContactsDBContext(): base("ContactsDBContext") 
        {
            Database.SetInitializer(new InitialDatabase());

        }

        public DbSet<Phone> phones { get; set; }
        public DbSet<Email> emails { get; set; }
        public DbSet<Tag> tags { get; set; }
        public DbSet<TagConnection> pairs { get; set; }

        public System.Data.Entity.DbSet<HRCloudTest.Models.Contact> Contacts { get; set; }

        public static List<Contact> GetContacts()
        {
            ContactsDBContext db = new ContactsDBContext();        
            List<Contact> contacts = new List<Contact>();
            contacts = db.Contacts.ToList();
            foreach(var c in contacts){
                ContactsDBContext.UpdateContact(c);
            }
            return contacts;
        }

        public static Contact UpdateContact(Contact c)
        {
            ContactsDBContext db = new ContactsDBContext();
            c.phones = (from p in db.phones where p.ownerId == c.id select p).ToList();
            c.emails = (from e in db.emails where e.ownerId == c.id select e).ToList();
            return c;
        }

    }
}