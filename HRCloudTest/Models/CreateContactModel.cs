﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace HRCloudTest.Models
{
    public class CreateContactModel
    {
        [Required]
        [Display(Name = "First name")]
        public string name { get; set; }

        [Required]
        [Display(Name = "Last name")]
        public string surname { get; set; }

        [Required]
        public string address { get; set; }

        //[EmailAddress(ErrorMessage = "Invalid email!")]
        [DataType(DataType.EmailAddress)]
        [Required]
        public string email { get; set; }

        [Display(Name = "Phone number")]
        [Required]
        [DataType(DataType.PhoneNumber)]
        public string phone { get; set; }

        public string tags { get; set; }
    }
}