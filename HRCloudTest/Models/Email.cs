﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;

namespace HRCloudTest.Models
{
    public class Email
    {
        

        public int id { get; set; }
        public String emailAddress { get; set; }
        [Display(Name = "Is primary")]
        public Boolean isPrimary { get; set; }

        public int ownerId { get; set; }

        public Email(String emailAddress)
        {
            this.emailAddress= emailAddress;
        }

        public Email() { }

        public bool Equals(Email other)
        {
            if(this.emailAddress == null && other.emailAddress != null )
            {
                return false;
            }
            return this.emailAddress.Equals(other.emailAddress);
        }

        public override bool Equals(object obj)
        {
            if(obj as Email == null)
            {
                return base.Equals(obj);
            } else
            {
                return this.Equals(obj as Email);
            }
        }

    }
}