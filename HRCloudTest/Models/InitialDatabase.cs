﻿using HRCloudTest.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;


namespace HRCloudApplication.Models
{
    public class InitialDatabase : System.Data.Entity.DropCreateDatabaseAlways<ContactsDBContext>
    {
        protected override void Seed(ContactsDBContext db)
        {
            for (int i = 0; i < 100; ++i)
            {
                Contact c = new Contact("name" + i, "surname" + i, "address" + i);
                db.Contacts.Add(c);
                db.SaveChanges();
                for (int j = 1; j < 3; ++j)
                {
                    Email e = new Email("email" + i + (j - 1) * 100 + "@hrcloud.hr");
                    e.ownerId = c.id;
                    db.emails.Add(e);
                    Phone p = new Phone("0912223" + i + (j - 1) * 100);
                    p.ownerId = c.id;
                    db.phones.Add(p);
                    Tag tag = new Tag("tag" + i + (j - 1));
                    db.tags.Add(tag);
                    db.SaveChanges();
                    tag.connectionAdded(c.id);
                }
                db.SaveChanges();
                
            }
            
            base.Seed(db);
            db.SaveChanges();

        }
    }
}