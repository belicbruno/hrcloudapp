﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;


namespace HRCloudTest.Models
{
    public class Phone
    {
        [Key]
        public int id { get; set; }
        
        [Required]
        
        public String phoneNumber { get; set; }
        public Boolean isPrimary { get; set; }
        [Display(Name = "Is primary")]
        public int ownerId { get; set; }

        public Phone(String phoneNumber)
        {
            this.phoneNumber = phoneNumber;
        }

        public Phone() { }

        public bool Equals(Phone other)
        {
            return this.phoneNumber.Equals(other.phoneNumber);
        }

        public override bool Equals(object obj)
        {
            if(obj as Phone == null)
            {
                return base.Equals(obj);
            } else
            {
                return this.Equals(obj as Phone);
            }
        }
    }
}