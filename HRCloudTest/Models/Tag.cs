﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.Entity;
using System.ComponentModel.DataAnnotations;


namespace HRCloudTest.Models
{
    public class Tag
    {
        
        

        [Key]
        public int id { get; set; }
        public string tagText { get; set; }
        public int connectionNumber { get; set; } = 0;

        public Tag(string tagText)
        {
            this.tagText = tagText;
        }

        public Tag() { }

        public bool Equals(Tag other)
        {
            return this.tagText.Equals(other.tagText);
        }

        public override bool Equals(object obj)
        {
            if (obj as Tag == null)
            {
                return base.Equals(obj);
            }
            else
            {
                return this.Equals(obj as Tag);
            }
        }

        public void connectionAdded(int contactId)
        {
            TagConnection pair = new TagConnection(this.id, contactId);
            ContactsDBContext db = new ContactsDBContext();
            if (!db.pairs.ToList().Contains(pair))
            {
                connectionNumber++;
                db.pairs.Add(pair);
                db.Entry(this).State = EntityState.Modified;           
                db.SaveChanges();                
            }
        }

        public void connectionLost(int contactId)
        {
            ContactsDBContext db = new ContactsDBContext();
            db.pairs.Remove((from p in db.pairs where p.tagId == this.id && p.contactId==contactId select p).First());
            connectionNumber--;
            if (connectionNumber == 0)
            {
                db.tags.Remove(db.tags.Find(this.id));
            }
            try
            {
                db.SaveChanges();

            }
            catch { }
        }

        public static List<Tag> parseTags(string text)
        {
            string[] tags = text.Split(';').Distinct().ToArray();
            List<Tag> parsedTags = new List<Tag>();
            ContactsDBContext db = new ContactsDBContext();
            foreach (var t in tags)
            {
                if(t.Equals("") || t.Equals("undefined"))
                {
                    continue;
                }
                Tag tag = null;
                try
                {
                    tag = db.tags.ToList().Find(e => e.tagText == t);
                }
                catch { }
                if (tag == null)
                {
                    tag = new Tag(t);
                    db.tags.Add(tag);
                }
                parsedTags.Add(tag);
            }
            
            db.SaveChanges();
            return parsedTags;

        }

        public static String GetContactTags(int contactId)
        {
            String returnString = "";
            ContactsDBContext db = new ContactsDBContext();
            List<int> tagIds = (from p in db.pairs where p.contactId == contactId select p.tagId).ToList();
            foreach(var tagId in tagIds){
                returnString += db.tags.Find(tagId).tagText + ";";
            }
            return returnString;
        }
    }
}