﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.ComponentModel.DataAnnotations;
namespace HRCloudTest.Models
{
    public class TagConnection
    {
        
        public int id { get; set; }

        public int tagId { get; set; }

        public int contactId { get; set; }

        public TagConnection(int tagId, int contactId)
        {
            this.tagId = tagId;
            this.contactId = contactId;
        }

        public TagConnection() { }

        public bool Equals(TagConnection other)
        {
            return this.contactId == other.contactId && this.tagId == other.tagId;
        }

        public override bool Equals(object obj)
        {
            if (obj as TagConnection == null)
            {
                return this.Equals(obj);
            }
            else
            {
                return this.Equals(obj as TagConnection);
            }
        }
    }
}
