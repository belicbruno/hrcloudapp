﻿myApp.controller('contactsController', ['$scope', '$routeParams', 'contactsService','phonesService','emailsService', '$location', function ($scope, $routeParams, contactsService,phonesService,emailsService, $location) {





   


    $scope.getContacts = function () {
        contactsService.getContacts()
            .success(function (data) {
                $scope.contacts = data;
            })
            .error(function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
                console.log($scope.status);
            });
    }

    $scope.search;

    $scope.searchContacts = function (searchString) {
        contactsService.searchContacts(search)
            .success(function (data) {                
                $scope.contacts = (data);

            })
            .error(function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
                console.log($scope.status);
            });
    }

    
    if ($routeParams.contactId != '' && $routeParams.contactId != null) {
        contactsService.getDetails($routeParams.contactId)
            .success(function (data) {
                $scope.contact = data;
            })
            .error(function (error) {
                confirm("data load failed");
            });
        contactsService.getPhones($routeParams.contactId)
            .success(function (data) {
                $scope.phones = data;
            }).error(function(error){
                confirm("phone"+error);
            });

        contactsService.getEmails($routeParams.contactId)
            .success(function (data) {
                $scope.emails = data
            }).error(function(error){
                confirm("email" + error);
            });
    } else if ($routeParams.searchString != '' && $routeParams.searchString != null && routeParams.searchString != "{{search}}") {
        confirm($routeParams.searchString);
        $scope.searchContacts($scope.routeParams.searchString);
    } else {
        $scope.getContacts();
    }
    

   
    

    $scope.create = function (name, surname, address, email, phone, tags) {
        contactsService.create(name, surname, address, email, phone, tags)
            .success(function (data) {
                if (data == 'True') {
                    $location.url('/contacts/');
                }
                else {
                    confirm(data);
                }
            })
    };

    $scope.edit = function (id,name, surname, address, tags) {
        contactsService.edit(id,name, surname, address, tags)
            .success(function (data) {
                if (data == 'True') {
                    confirm("Contact updated");
                    $location.url('/contacts/');
                }

                else {
                    confirm(data);
                }
            }).error(function (error) {
                confirm(error);
            });
    };

    $scope.delete = function (id) {
        contactsService.delete(id)
            .success(function (data) {
                if (data == 'True') {
                    $location.url('/contacts');
                }
                else {
                    confirm("Error: cannot delete contact!");
                }
            })
    };

    $scope.deletePhone = function (phoneId) {
        phonesService.delete(phoneId)
            .success(function (data) {
                if (data == 'True') {
                    phonesService.getPhones($routeParams.contactId).success(function (data){
                        $scope.phones=data;
                    })
                }
                else {
                    confirm("Cannot delete phone. At least one phone is required!");
                }
            })
    };

    $scope.deleteEmail = function (emailId) {
        emailsService.delete(emailId)
            .success(function (data) {
                if (data == 'True') {
                    emailsService.getEmails($routeParams.contactId).success(function(data){
                        $scope.emails = data;
                    })
                }
                else {
                    confirm("Cannot delete phone. At least one phone is required!");
                }
            })
    };

    $scope.searchContacts = function (searchString) {
        contactsService.searchContacts(searchString)
            .success(function (data) {
                $scope.contacts = data;
            })
            .error(function (error) {
                $scope.status = 'Unable to load data: ' + error.message;
                console.log($scope.status);
            });
    }

    if ($routeParams.searchString != '' && $routeParams.searchString != null) {
        $scope.searchContacts(searchString);
    }


    
}]);