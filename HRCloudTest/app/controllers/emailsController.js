﻿myApp.controller('emailsController', ['$scope', '$routeParams', 'emailsService', '$location', function ($scope, $routeParams, emailsService, $location) {


    $scope.getEmails = function (ownerId) {
        emailService.getEmails(ownerId)
            .success(function (data) {
                $scope.emails = data;

            })
            .error(function (error) {
                confirm(error)
            });
    }

    $scope.getEmail = function (emailId) {
        emailsService.getEmail(emailId)
            .success(function (data) {
                $scope.email = data;

            }).error(function (error) {
                confirm(error);
            });
    }


    if ($routeParams.emailId != "" && $routeParams.emailId != null) {
        $scope.getEmail($routeParams.emailId)
    }

    $scope.addEmail = function (emailAddress) {
        emailsService.addEmail(emailAddress, $routeParams.ownerId)
            .success(function (data) {
                if (data == 'True') {
                    $location.url('/contacts/edit/' + $routeParams.ownerId);
                }
                else
                    confirm(data);
            })
    };

    $scope.editEmail = function (emailAddress, ownerId) {
        emailsService.editEmail($routeParams.emailId,emailAddress)
            .success(function (data) {
                if (data == 'True') {
                    $location.url('/contacts/edit/' + ownerId);
                }
                else
                    confirm(data);
            }).error(function (error) {
                confirm(error);
            });
    };




    $scope.delete = function (emailId) {
        emailsService.delete(emailId)
            .success(function (data) {
                if (data == 'True') {
                    $scope.getEmails($routeParams.ownerId);
                }
                else {
                    confirm("Cannot delete phone. At least one phone is required!");
                }
            })
    };

    $scope.change = function (emailId) {
        emailsService.change(emailId)
            .success(function (data) {
                if (data == 'True') {
                    $scope.getEmails($routeParams.idContact);
                }
                else
                    confirm("Cannot change to primary number!");
            })
    };

    

}]);