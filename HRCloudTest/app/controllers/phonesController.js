﻿myApp.controller('phonesController', ['$scope', '$routeParams', 'phonesService', '$location', function ($scope, $routeParams, phonesService, $location) {

  
   
    
    $scope.getPhones = function (ownerId) {
        phonesService.getPhones(ownerId)
            .success(function (data) {
                $scope.phones = data;             
            })
            .error(function (error) {
                $scope.status = 'Error: ' + error.message;
                console.log($scope.status);
            });
    }

    $scope.getPhone = function (phoneId) {
        phonesService.getPhone(phoneId)
            .success(function (data) {
                $scope.phone = data;
                //confirm(phone.id);
               
            })
            .error(function (error) {
                confirm(error)
            });
    }

    if ($routeParams.phoneId != "" && $routeParams.phoneId != null) {
        $scope.getPhone($routeParams.phoneId)
    }

    $scope.addPhone = function (phoneNumber) {
        phonesService.addPhone(phoneNumber,$routeParams.ownerId)
            .success(function (data) {
                if (data == 'True') {
                    $location.url('/contacts/edit/'+$routeParams.ownerId);
                }
                else {
                    confirm(data);
                }
            })
    };

    $scope.editPhone = function (phoneNumber,ownerId) {
        phonesService.editPhone($routeParams.phoneId, phoneNumber)
            .success(function (data) {
                if (data == 'True') {
                    $location.url('/contacts/edit/' + ownerId );
                }
                else {
                    confirm(data);
                }
            }).error(function(error) {
                confirm(error);
            }   )
    };

    
   

    $scope.delete = function (phoneId) {
        phonesService.delete(phoneId)
            .success(function (data) {
                if (data == 'True') {
                    $scope.getPhones($routeParams.ownerId);
                }
                else {
                    confirm("Cannot delete phone. At least one phone is required!");
                }
            })
    };  


    $scope.change = function (phoneId) {
        phonesService.change(phoneId)
            .success(function (data) {
                if (data == 'True') {
                    $scope.getPhones($routeParams.idContact);
                }
                else
                    confirm("Cannot change to primary number!");
            })
    };

   

}]);