﻿var myApp = angular.module('myApp', ['ngRoute']);
myApp.config(['$routeProvider', function ($routeProvider) {
    $routeProvider
        .when('/contacts/', {
            controller: 'contactsController',
            templateUrl: '/HTMLs/contacts_list.html'
        }).when('/contacts/create/', {
            controller: 'contactsController',
            templateUrl: '/HTMLs/contact_create.html'
        }).when('/contacts/edit/:contactId/', {
            controller: 'contactsController',
            templateUrl: '/HTMLs/contact_edit.html'
        }).when('/contacts/details/:contactId/', {
            controller: 'contactsController',
            templateUrl: '/HTMLs/contact_details.html'
        }).when('/contacts/delete/:contactId/', {
            controller: 'contactsController',
            templateUrl: 'HTMLs/contact_delete.html'
        }).when('/phones/create/:ownerId', {
            controller: 'phonesController',
            templateUrl: '/HTMLs/phone_create.html'
        }).when('/phones/edit/:phoneId', {
            controller: 'phonesController',
            templateUrl: '/HTMLs/phone_edit.html'
        }).when('/emails/create/:ownerId', {
            controller: 'emailsController',
            templateUrl: '/HTMLs/email_create.html'
        }).when('/emails/edit/:emailId', {
            controller: 'emailsController',
            templateUrl: '/HTMLs/email_edit.html'
        }).when('/contacts/search/:searchString', {
            controller: 'contactsController',
            templateUrl: '/HTMLs/contacts_list.html'
        }).otherwise({
            redirectTo: '/contacts/'
        });
}]);