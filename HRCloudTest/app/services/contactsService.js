﻿myApp.factory('contactsService', ['$http', function ($http) {
    var contactsService = {};

    contactsService.getContacts = function () {
        return $http.get('/Contacts/Index/');
    }

    contactsService.getDetails = function (param) {
        return $http.get('/Contacts/Details/' + param);
    }

    contactsService.create = function (name, surname, address, email, phone, tags) {
        return $http.get('/Contacts/Create/?name=' + name + '&surname=' + surname + '&address=' + address + '&email=' + email + '&phone=' + phone + '&tags=' + tags)
    }

    contactsService.edit = function (id, name, surname, address,tags) {
        return $http.get('/Contacts/Edit/?id=' + id + '&name=' + name + '&surname=' + surname + '&address=' + address +'&tags=' + tags)
    }

    contactsService.delete = function (id) {
        return $http.get('/Contacts/Delete/?id=' + id);
    }

    contactsService.getPhones = function (ownerId) {
        return $http.get('/Phones/GetAll/' + ownerId);
    }
    contactsService.getEmails = function (ownerId) {
        return $http.get('/Emails/GetAll/' + ownerId);
    }

    contactsService.searchContacts = function (searchString) {
        return $http.get('/Contacts/Filter/?searchString=' + searchString);
    }
    

    return contactsService;
}]);