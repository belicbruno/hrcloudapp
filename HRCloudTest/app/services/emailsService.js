﻿myApp.factory('emailsService', ['$http', function ($http) {
    var emailsService = {};

    emailsService.getEmails = function (ownerId) {
        return $http.get('/Emails/GetAll/' + ownerId);
    }

    emailsService.getEmail = function (emailId) {
        return $http.get('/Emails/Get/' + emailId);
    }

   

    emailsService.addEmail = function (emailAddress,ownerId) {
        return $http.get('/Emails/Create/?emailAddress=' + emailAddress + '&ownerId='  + ownerId)
    }

   

    emailsService.editEmail = function (id,emailAddress) {
        return $http.get('/Emails/Edit/?id=' + id + '&emailAddress=' + emailAddress )
    }
    

    emailsService.delete = function (emailId) {
        return $http.get('/Emails/Delete/' + emailId);
    }

    emailsService.change = function (idPhone) {
        return $http.get('/Emails/Primary/' + idPhone);
    }

    return emailsService;
}]);