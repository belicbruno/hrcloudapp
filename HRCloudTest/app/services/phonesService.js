﻿myApp.factory('phonesService', ['$http', function ($http) {
    var phonesService = {};

    phonesService.getPhones = function (ownerId) {
        return $http.get('/Phones/GetAll/' + ownerId);
    }

    phonesService.getPhone = function (phoneId) {
        return $http.get('/Phones/Get/' + phoneId);
    }

    phonesService.addPhone = function (phoneNumber,ownerId) {
        return $http.get('/Phones/Create/?phoneNumber=' + phoneNumber + '&ownerId=' + ownerId)
    }

    phonesService.editPhone = function (id,phoneNumber) {
        return $http.get('/Phones/Edit/?id=' + id + '&phoneNumber=' + phoneNumber)
    }

    phonesService.delete = function (phoneId) {
        return $http.get('/Phones/Delete/' + phoneId);
    }

    phonesService.change = function (idP) {
        return $http.get('/Phones/Primary/' + idPhone);
    }


    return phonesService;
}]);